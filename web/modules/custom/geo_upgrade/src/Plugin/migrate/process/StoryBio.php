<?php


namespace Drupal\geo_upgrade\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "geo_story_bio"
 * )
 */
class StoryBio extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $result = [];
    foreach ($value[0] as $key => $item) {
      $result[$key] = $item;
      $result[$key]['bio'] = isset($value[1][0]['value']) ? $value[1][0]['value'] : '';
    }
    return $result;
  }
}
