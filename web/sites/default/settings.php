<?php

$settings['locale_custom_strings_en'][] = [
  'Sticky at top of lists'      => 'Feature on front page',
];


// #ddev-generated: Automatically generated Drupal settings file.
if (file_exists($app_root . '/' . $site_path . '/settings.ddev.php')) {
  include $app_root . '/' . $site_path . '/settings.ddev.php';
}
