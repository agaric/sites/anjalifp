// From https://benmarshall.me/responsive-iframes/
// Note that the JavaScript approach does not combine with his recommended CSS
// approach, and it would probably be better/smoother to combine them.
// @TODO Or, best, write a text formatter that does the below and assigns the
// appropriate class, for example .embed-responsive-4by3

(function ($) {
  'use strict';

  // Find all iframes
  var $iframes = $( "iframe" );

  // Find and save the aspect ratio for all iframes
  $iframes.each(function () {
    $( this ).data( "ratio", this.height / this.width )
      // Remove the hardcoded width &amp;amp;#x26; height attributes
      .removeAttr( "width" )
      .removeAttr( "height" );
  });

  // Resize the iframes when the window is resized
  $( window ).resize( function () {
    $iframes.each( function() {
      // Get the parent container&amp;amp;#x27;s width
      var width = $( this ).parent().width();
      $( this ).width( width )
        .height( width * $( this ).data( "ratio" ) );
    });
  // Resize to fix all iframes on page load.
  }).resize();

})(jQuery);

// Note that we know we have JQuery because our geofresco/global relies on bulma/global
// which pulls in JQuery already.
